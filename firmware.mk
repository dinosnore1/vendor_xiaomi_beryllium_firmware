# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/beryllium/firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/beryllium/firmware/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/xiaomi/beryllium/firmware/aop.img:install/firmware-update/aop.img \
    vendor/xiaomi/beryllium/firmware/devcfg.img:install/firmware-update/devcfg.img \
    vendor/xiaomi/beryllium/firmware/qupfw.img:install/firmware-update/qupfw.img \
    vendor/xiaomi/beryllium/firmware/tz.img:install/firmware-update/tz.img \
    vendor/xiaomi/beryllium/firmware/storsec.img:install/firmware-update/storsec.img \
    vendor/xiaomi/beryllium/firmware/keymaster.img:install/firmware-update/keymaster.img \
    vendor/xiaomi/beryllium/firmware/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/xiaomi/beryllium/firmware/xbl.img:install/firmware-update/xbl.img \
    vendor/xiaomi/beryllium/firmware/modem.img:install/firmware-update/modem.img \
    vendor/xiaomi/beryllium/firmware/xbl_config.img:install/firmware-update/xbl_config.img \
    vendor/xiaomi/beryllium/firmware/dsp.img:install/firmware-update/dsp.img \
    vendor/xiaomi/beryllium/firmware/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi/beryllium/firmware/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/xiaomi/beryllium/firmware/hyp.img:install/firmware-update/hyp.img